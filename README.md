# Hotspur setup instruction

## Requirements

- A Linux computer with at least one NVIDIA GPU (4x GTX1080 recommended)
- Docker with API > 1.4 [Link](https://docs.docker.com/)
- Docker-compose [Link](https://docs.docker.com/compose/)
- nvidia-container-toolkit [Link](https://github.com/NVIDIA/nvidia-docker/wiki/Installation-\(Native-GPU-Support\))
- It is highly recommended to setup a user with docker privileges and use it for the following instructions, otherwise they need to be performed as root
- An SSD with at least 4TB of space is recommended
- Data collected by SerialEM, with the .mdoc for every file option enabled

## Download required software

On your SSD download the hotspru:
```
git clone https://gitlab.com/hotspur-cryoem/setup.git hotspur
cd hotspur/
```

In this directory download the following files: 
- [ctffind-4.1.13-linux64.tar.gz](https://grigoriefflab.umassmed.edu/ctf_estimation_ctffind_ctftilt)
- [MotionCor2_1.3.0.zip](https://emcore.ucsf.edu/ucsf-motioncor2)
- [imod_4.9.12_RHEL6-64_CUDA8.0.sh](https://bio3d.colorado.edu/imod/download.html)

## Edit the configuration file

- Edit the config/hotspur-config.yml file, especially these options:

```
# Folder where raw data from SerialEM is saved:
raw_data_folder: /nfsserver/rawdata/

# Salt used when hashing session and project names
hash_salt: change_this_salt

# The ids of the GPUs available to hotspur for processing
gpus: [0,1,2,3]

# The maximum number of threads hotspur will use for processing
threads: 6

# Admin credentials for the couchdb instance
admin_name: admin
admin_pass: change_this_password

# The max age (in days) of sessions that hotspur will process
# Use a short maximum age to avoid processing a large backlog
session_max_age: 4
```
Hotspur by default expects that the  raw_data_folder contains subfolders in the following scheme:

```
Project1/
    Grid1/
        Session1/
            navigator.nav
            lmm.mrc
            lmm.mrc.mdoc
            Frames/
                Gainref.dm4
                stack_0001.mrc
                stack_0001.mrc.mdoc
    Grid2/
Project2/

etc..
```

This scheme is somewhat flexible. If you have questions please let us know.

## Start the web interface

Run 

```
./hotspur_setup launch-backend
```


Point you browser to http://localhost/web-view/ and you should get a message that data has not been loaded yet/

To test the database run:

```
./hotspur_setup test-backend
```

The last line of the result should look like this:

```
{"couchdb":"Welcome","version":"2.3.1","git_sha":"c298091a4","uuid":"1aa526ff9918ff8058212764e75e637c","features":["pluggable-storage-engines","scheduler"],"vendor":{"name":"The Apache Software Foundation"}}
```

## Create Hotspur processing container

- Run:
```
./hotspur_setup build-container
```

## Run Hotspur

To run dataprocessing run 
```
./hotspur process
```

To get the urls for the webinterface run
```
./hotspur info
```

# FAQ

## When launching the backend I get a ```docker-compose: command not found``` error, even though docker-compose is installed

You are probably trying to launch the backend using sudo. Ideally you would not do this, but instead enable docker commands for a normal user by adding the user to the group docker. 

If you want to use sudo anyway make sure /usr/local/bin is added to the secure_path in visudo.