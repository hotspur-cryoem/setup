Bootstrap: docker
From: nvidia/cuda:10.1-runtime-centos7

%files
ctffind-4.1.13-linux64.tar.gz /software/ctffind/
imod_4.9.12_RHEL6-64_CUDA8.0.sh /software/imod/
MotionCor2_1.3.0.zip /software/motioncor2/

%environment
IMOD_DIR=/software/imod/imod_4.9.12
PYTHON_VERSION=3.7.3
PATH=/opt/conda/bin:$PATH
export IMOD_DIR PYTHON_VERSION PATH

%post
yum install -y git unzip libtiff wget bzip2 mesa-libGL && rm -rf /var/cache/yum/*

cd /software

cd ctffind && tar -xzf ctffind-4.1.13-linux64.tar.gz && cd ..

cd imod && bash imod_4.9.12_RHEL6-64_CUDA8.0.sh -extract && tar -xzf IMODtempDir/imod_4.9.12_RHEL6-64_CUDA8.0.tar.gz && cd ..

cd motioncor2 && unzip MotionCor2_1.3.0.zip &&cd ..

wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-4.7.12-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh && \
    /opt/conda/bin/conda install -y python=$PYTHON_VERSION && \
    /opt/conda/bin/conda clean -tipsy && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    echo "conda activate hotspur" >> ~/.bashrc
cd /software
git clone https://gitlab.com/hotspur-cryoem/command.git hotspur_command
/opt/conda/bin/conda env create -f hotspur_command/templates/files/environment.yml -n hotspur

%runscript
source /opt/conda/etc/profile.d/conda.sh
conda activate hotspur
exec "$@"
