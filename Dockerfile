FROM nvidia/cuda:10.1-runtime-centos7
LABEL maintainer "Johannes Elferich <elferich@ohsu.edu>"

RUN yum install -y git unzip libtiff wget bzip2 mesa-libGL && rm -rf /var/cache/yum/*

WORKDIR /software

COPY ctffind-4.1.13-linux64.tar.gz /software/ctffind/
RUN cd ctffind && tar -xzf ctffind-4.1.13-linux64.tar.gz

COPY imod_4.9.12_RHEL6-64_CUDA8.0.sh /software/imod/
RUN cd imod && bash imod_4.9.12_RHEL6-64_CUDA8.0.sh -extract && tar -xzf IMODtempDir/imod_4.9.12_RHEL6-64_CUDA8.0.tar.gz
ENV IMOD_DIR /software/imod/imod_4.9.12

COPY MotionCor2_1.3.1.zip /software/motioncor2/
RUN cd motioncor2 && unzip MotionCor2_1.3.1.zip

ENV PYTHON_VERSION=3.7.3
ENV PATH /opt/conda/bin:$PATH
RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-4.7.12-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh && \
    /opt/conda/bin/conda install -y python=$PYTHON_VERSION && \
    /opt/conda/bin/conda clean -tipsy && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    echo "conda activate hotspur" >> ~/.bashrc

ARG HOTSPUR_VER=unknown
RUN  git clone https://gitlab.com/hotspur-cryoem/command.git hotspur_command

RUN conda env create -f hotspur_command/templates/files/environment.yml -n hotspur

ENTRYPOINT ["./hotspur_command/entrypoint.sh"]
